<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>PHPMailer - mail() test</title>
</head>
<body>
<?php
require 'omdomme/class.phpmailer.php';

//Create a new PHPMailer instance
$mail = new PHPMailer();
//Set who the message is to be sent from
$mail->SetFrom('webmaster@apeland.no', 'Webmaster Koch');
//Set who the message is to be sent to
$mail->AddAddress('willy@apeland.no', 'Willy Koch');
//Set the subject line
$mail->Subject = 'Test fra intern server';
//Read an HTML message body from an external file, convert referenced images to embedded, convert HTML into a basic plain-text alternative body
$mail->MsgHTML(file_get_contents('contents.html'), dirname(__FILE__));
//Replace the plain text body with one created manually
$mail->AltBody = 'Dette er ren tekst';
//Attach an image file

//Send the message, check for errors
if(!$mail->Send()) {
  echo "Mailer Error: " . $mail->ErrorInfo;
} else {
  echo "Message sent!";
}
?>
</body>
</html>